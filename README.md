README

# 이 프로젝트는

사내 워크샵 프로그램입니다.

팀원 만들기, 게임 선택 페이지로 구성되어있습니다.

## Installation

### install node
[nodejs 다운로드](https://nodejs.org/en/download/)

### vue 플러그인 설치

### nodejs 설정
```bash
Languages & Frameworks > node.js
  > Node interpreter : C:\Program Files\nodejs\node.exe (nodejs 설치 경로로 설정)
  > Package manager : C:\Program Files\nodejs\npm.cmd (nodejs 설치 경로로 설정)
```

## 오류 확인

```bash
npm run lint
```
