/**
 * 기본사용법
 * 상단페이지에서 사용할 변수 아래와 같이 import
 * import { glbCdnUrl, add } from '@/components/module/common.js'
 */

import router from '@/router'

const storage = {
    getMembersKey : function () {
        return 'members';
    },
    getTeamsKey : function () {
        return 'teams';
    },
    getTeamCountKey : function () {
        return 'teamCount';
    },
    getTeamTypeKey : function () {
        return 'teamType';
    },
    getTeamMasterKey : function () {
        return 'teamMaster';
    },
    getNumbersKey : function () {
        return 'numbers';
    }
}

function movePageByName (name) {
    if (!isNull(name)) {
        router.push({name: name}).then();
    }
}

function isNull (str) {
    return !(str && str.length > 0);
}

function clearStorage() {
    setMembers(null);
    setTeams(null);
    setTeamType('R');
    setTeamMaster(null);
}

function setMembers(members) {
    localStorage.setItem(storage.getMembersKey(), JSON.stringify(members));
}

function getMembers() {
    return JSON.parse(localStorage.getItem(storage.getMembersKey()));
}

function setTeams(teams) {
    localStorage.setItem(storage.getTeamsKey(), JSON.stringify(teams));
}

function getTeams() {
    return JSON.parse(localStorage.getItem(storage.getTeamsKey()));
}

function setTeamCount(teamCount) {
    localStorage.setItem(storage.getTeamCountKey(), teamCount);
}

function getTeamCount() {
    return localStorage.getItem(storage.getTeamCountKey());
}

function setTeamType(teamType) {
    localStorage.setItem(storage.getTeamTypeKey(), teamType);
}

function getTeamType() {
    return localStorage.getItem(storage.getTeamTypeKey());
}

function setTeamMaster(teamMaster) {
    localStorage.setItem(storage.getTeamMasterKey(), JSON.stringify(teamMaster));
}

function getTeamMaster() {
    return JSON.parse(localStorage.getItem(storage.getTeamMasterKey()));
}

function setNumbers(numbers) {
    localStorage.setItem(storage.getNumbersKey(), JSON.stringify(numbers));
}

function getNumbers() {
    return JSON.parse(localStorage.getItem(storage.getNumbersKey()));
}

export { movePageByName, clearStorage, setMembers, getMembers, setTeams, getTeams, setTeamCount, getTeamCount, setTeamType, getTeamType, setTeamMaster, getTeamMaster, setNumbers, getNumbers }
