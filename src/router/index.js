import Vue from 'vue'
import Router from 'vue-router'

import pageNotFound from "@/views/error/pageNotFound";
import makeTeam from "@/views/board/makeTeam";
import makeMember from "@/views/board/makeMember";
import makeNumber from "@/views/board/makeNumber";
import selectGame from "@/views/board/selectGame";

import body from "@/views/game/body"
import brand from "@/views/game/brand"
import inital from "@/views/game/inital"
import keep from "@/views/game/keep"
import music from "@/views/game/music"
import personal from "@/views/game/personal"
import wordEndWith from "@/views/game/wordEndWith"

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/404',
            name: 'pageNotFound',
            component: pageNotFound
        },
        {
            path: '/',
            redirect: '/makeMember'
        },
        {
            path: '/makeMember',
            name: 'makeMember',
            component: makeMember
        },
        {
            path: '/makeTeam',
            name: 'makeTeam',
            component: makeTeam
        },
        {
            path: '/makeNumber',
            name: 'makeNumber',
            component: makeNumber
        },
        {
            path: '/selectGame',
            name: 'selectGame',
            component: selectGame
        },
        {
            path: '/game/body',
            name: 'body',
            component: body
        },
        {
            path: '/game/brand',
            name: 'brand',
            component: brand
        },
        {
            path: '/game/inital',
            name: 'inital',
            component: inital
        },
        {
            path: '/game/keep',
            name: 'keep',
            component: keep
        },
        {
            path: '/game/music',
            name: 'music',
            component: music
        },
        {
            path: '/game/personal',
            name: 'personal',
            component: personal
        },
        {
            path: '/game/wordEndWith',
            name: 'wordEndWith',
            component: wordEndWith
        },

    ]
})