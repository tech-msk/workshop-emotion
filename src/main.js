import Vue from 'vue'
import App from './App'
import router from './router'

require('es6-promise').polyfill()

//window 객체에 scroll & mouse 이벤트를 추가하고 cursor함수 실행되도록 함
// window.addEventListener("scroll", cursor2);
window.addEventListener("wheel", cursor2);
window.addEventListener("mousemove", cursor);
//커스텀 커서의 left값과 top값을 커서의 XY좌표값과 일치시킴
function cursor(e) {
    let mouseCursor = document.querySelector(".cursor");
    mouseCursor.style.left = `${e.pageX}px`;
    mouseCursor.style.top = `${e.pageY + scrollY}px`;
}
function cursor2(e) {
    let mouseCursor = document.querySelector(".cursor");
    mouseCursor.style.left = `${e.pageX}px`;
    mouseCursor.style.top = `${e.pageY + scrollY}px`;
}

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')